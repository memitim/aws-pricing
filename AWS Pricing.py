
# coding: utf-8

# In[2]:


import boto3
import json
import pprint

pricing = boto3.client('pricing')


# In[3]:


print("All Services")
print("============")
response = pricing.describe_services()
for service in response['Services']:
    print(service['ServiceCode'] + ": " + ", ".join(service['AttributeNames']))
print()


# In[4]:


print("Selected EC2 Attributes & Values")
print("================================")
response = pricing.describe_services(ServiceCode='AmazonEC2')
attrs = response['Services'][0]['AttributeNames']

for attr in attrs:
    response = pricing.get_attribute_values(ServiceCode='AmazonEC2', AttributeName=attr)

    values = []
    for attr_value in response['AttributeValues']:
        values.append(attr_value['Value'])

    print("  " + attr + ": " + ", ".join(values))


# In[7]:


print(pricing.describe_services(ServiceCode='AmazonEC2'))


# In[21]:


pricing.get_attribute_values(ServiceCode='AmazonEC2', AttributeName='purchaseOption')


# In[24]:


print("Selected EC2 Products")
print("=====================")

response = pricing.get_products(
     ServiceCode='AmazonEC2',
     Filters = [
         {'Type' :'TERM_MATCH', 'Field':'operatingSystem', 'Value':'Windows'              },
         {'Type' :'TERM_MATCH', 'Field':'preInstalledSw',  'Value':'SQL Ent'              },
         {'Type' :'TERM_MATCH', 'Field':'location',        'Value':'US East (N. Virginia)'},
         {'Type' :'TERM_MATCH', 'Field':'termType',        'Value':'Reserved'             },
         {'Type' :'TERM_MATCH', 'Field':'productFamily', 'Value':'Compute Instance'              },
         {'Type' :'TERM_MATCH', 'Field':'purchaseOption', 'Value':'AllUpfront'              }
     ],
     MaxResults=100
)

for price in response['PriceList']:
 pp = pprint.PrettyPrinter(indent=1, width=300)
 pp.pprint(json.loads(price))
 print()


# In[38]:


(json.loads(price))['product']['attributes']


# In[39]:


print((json.loads(price))['product']['attributes']['instanceType'])
print((json.loads(price))['product']['attributes']['vcpu'])
print((json.loads(price))['product']['attributes']['memory'])
print((json.loads(price))['product']['attributes']['dedicatedEbsThroughput'])


# In[41]:


(json.loads(price))['terms']['Reserved']


# In[80]:


instanceList = []
for price in response['PriceList']:
    attys=(json.loads(price))['product']['attributes']
    instanceList.append([attys['instanceType'], attys['vcpu'], attys['memory']])


# In[82]:


instanceList[5]

